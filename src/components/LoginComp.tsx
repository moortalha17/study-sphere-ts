import React, { useEffect, useReducer } from "react";
import { Button } from "@/components/ui/button";
import Image from "next/image";
import GoogleIcon from "../assets/svgs/google-icon.svg";
import logo from '@/assets/svgs/logo.svg'
import { useSelector, useDispatch } from "react-redux";
import {
  logIn,
  logOut,
  
} from "@/features/userstate/userStateSlice";
import {
  getAuth,
  getRedirectResult,
  GoogleAuthProvider,
  signInWithPopup,
  signInWithRedirect,
} from "firebase/auth";
import { redirect, useRouter } from "next/navigation";
import {app} from "@/app/firebaseConfig";

const LoginComp = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    const auth = getAuth(app);

    const unSubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch(logIn(user));
      } else {
        dispatch(logOut());
      }
    });
    return () => unSubscribe();
  }, [dispatch]);

  async function continueWithGoogle() {
    const auth = getAuth(app);
    const provider = new GoogleAuthProvider();
  
    try {
     
      const result = await signInWithPopup(auth, provider) 
      const user = result.user
      if(!user){
        window.location.href = "/"
      }  
      if (user) {
        
        const token = await user.getIdToken();
        const response = await fetch('http://localhost:8080/api/storeUser', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ token }),
        });
       
        const responseData = await response.json();
        
        if (response.ok) {
          window.location.href = `/dashboard/${user.email}`
          
         
          
        } else {
          console.error('Error saving user data', responseData);
        }
      }
    } catch (error : any) {
      console.error('Error signing in with Google:', error.message);
    }
  }
  
  return (
    <>
      {/* Navigation */}
      <header className="max-w-screen">
            <nav className="mx-auto max-w-7xl p-6 flex drop-shadow-sm font-medium sticky z-10 justify-between">
              <div className='flex items-center gap-3'>
            <Image src={logo} alt="logo" width={25} height={25}/>
              <span>StudySphere</span>
              </div>
              
            </nav>
            <hr className="mx-auto md:mx-6 sm:mx-6 xs:mx-6 h-px border-t-0 bg-zinc-500 opacity-25 " />
          </header>

      {/* Main Content */}
      <div className="h-screen flex justify-center bg-white">
        <div className="max-w-[20rem] md:max-w-[30rem] h-full flex flex-col space-y-10 pt-20">
          {/* Login Section */}
          <div className="flex flex-col items-center justify-center space-y-6">
            <span className="text-4xl font-medium md:text-5xl ">Log in</span>

            {/* Google Login Button */}
            <Button
              onClick={continueWithGoogle}
              variant="outline"
              className="w-[20rem] flex space-x-3 items-center justify-center bg-white hover:bg-zinc-100 outline outline-1 outline-zinc-300"
            >
            
              <Image
                src={GoogleIcon}
                alt="Google Icon"
                width={15}
                height={15}
                className="bg-transparent"
              />
              <span className="bg-transparent">Continue with Google</span>
            </Button>
          </div>

          {/* Divider */}
          <hr className="  bg-neutral-500  " />

          {/* Terms and Conditions */}
          <div className="text-xs text-zinc-400 text-center">
            <span className="text-black">
              By clicking “Continue with Google” above, you acknowledge that you
              have read and understood, and agree to StudySphere's
            </span>
            &nbsp;
            <button className="transition duration-200 hover:text-[#3ECF8E] ease-in-out">
              <a className="underline ">Terms & Conditions</a>
            </button>
            <span> and </span>
            <button className="transition duration-200 hover:text-[#3ECF8E] ease-in-out">
              <a className="underline">Privacy Policy</a>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginComp;
