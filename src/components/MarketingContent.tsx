import React, { useState, useEffect } from "react";
import SectionContainer from '../layouts/SectionContainer'
import { Button } from "./ui/button";
import { usePathname } from "next/navigation";
import localFont from 'next/font/local'
import github from '@/assets/svgs/github.svg'
import Image from "next/image";
const myFont = localFont({ src: '../assets/fonts/CircularStd-Book.otf' })

const MarketingContent = () => {
  const [isLoading, setIsLoading] = useState(false);
  const pathname = usePathname();
  const handleClick = () => {
    if (pathname === "/") {
      setIsLoading(true);
    } else {
      setIsLoading(false);
    }
  };

  return (
    <div className={`relative -mt-[65px]  `}>
    <SectionContainer className="pt-8 md:pt-16 overflow-hidden ">
      <div className="relative">
        <div className="mx-auto">
          <div className="mx-auto max-w-2xl lg:col-span-6 lg:flex lg:items-center justify-center text-center">
            <div className="relative z-10 lg:h-auto pt-[90px] lg:pt-[90px] lg:min-h-[300px] flex flex-col items-center justify-center sm:mx-auto md:w-3/4 lg:mx-0 lg:w-full gap-4 lg:gap-8">
              <div className="flex flex-col items-center">
                <div className="z-40 w-full flex justify-center -mt-4 lg:-mt-12 mb-8">
                  
                </div>
                <h1 className="text-foreground text-4xl sm:text-5xl sm:leading-none lg:text-7xl">
                  <span className="block text-black bg-clip-text font-bold">
                    Create in an hour
                  </span>
                  <span className="text-transparent bg-clip-text bg-gradient-to-br from-[#3ECF8E] via-[#3ECF8E] to-[#3ecfb2] block md:ml-0 font-bold">
                    Scale to millions
                  </span>
                </h1>
                <p className="pt-2 text-zinc-800 my-3 text-sm sm:mt-5 lg:mb-0 sm:text-base lg:text-lg tracking-normal">
                  StudySphere is a virtual classroom.{' '}
                  <br className="hidden md:block" />
                  Start your learning or teaching journery with our best product, that gives every thing you need to manage a classroom or to join a classroom.
                </p>
              </div>
              <div className="flex items-center gap-2">
              <Button className="tracking-normal w-32 drop-shadow-lg bg-[#37986A] hover:bg-[#2D7753] relative border border-green-400 h-10">
            <a className="bg-transparent text-white" href="/login">Get Started</a>
          </Button>
          <Button className="tracking-noraml w-28 bg-white hover:bg-zinc-100 relative outline outline-2 outline-zinc-200 shadow-sm h-10 shadow flex items-center gap-2">
         <Image src={github} alt="" width={14} className="relative bottom-[0rem]"/>
            <a className="bg-transparent text-black" href="https://github.com/Talha2112">Github</a>
          </Button>
              </div>

              
            </div>
          </div>
        </div>
      </div>
    </SectionContainer>
  </div>
  );
};

export default MarketingContent;
