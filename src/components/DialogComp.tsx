"use client";
import React, { useState } from "react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { getAuth } from "firebase/auth";
import { app } from "@/app/firebaseConfig";
import { useRouter } from "next/navigation";
import {  getToken } from "firebase/messaging"; 
import { useToast } from "@/components/ui/use-toast";

interface DialogCompProps {
  handlePopup: (argument: string) => void;
  isClicked: string;
  title: string;

}

export function DialogComp({ handlePopup, isClicked, title }: DialogCompProps) {

  const router = useRouter();



  function generateUniqueId(length: number) {
    return Math.random().toString(36).substr(2, length);
  }

  const [localFormData, setLocalFormData] = useState({
    classCode:'',
    className: '',
    subject: '',
    section: '',
  });
  const [loading, setLoading] = useState(false);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    setLocalFormData((prevData) => ({
      ...prevData,
      [id]: value,
    }));
  };

  const auth = getAuth(app);

  const handleSubmit = async (e: React.FormEvent) => {
    setLoading(true);
    e.preventDefault();
  
    const user = auth.currentUser;
  
    if (!user) {
      console.error('User is not authenticated');
      return;
    }
  
    try {
      const token = await user.getIdToken();

      let endpoint, updatedFormData;
  
      if (isClicked === "joinClass") {
        endpoint = 'http://localhost:8080/api/joinClass';
        updatedFormData = {
          classCode: localFormData.classCode,
          token,
          isJoined: true,
         
        };
      } else if (isClicked === "createClass") {
        endpoint = 'http://localhost:8080/api/createClass';
        const classCode = generateUniqueId(5);
        updatedFormData = {
          ...localFormData,
          classCode,
          token,
          isCreated: true,
         
        };
      } else {
        throw new Error('Invalid operation');
      }
  
      const response = await fetch(endpoint, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedFormData),
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log("Updated Data", updatedFormData);
        const savedForms = sessionStorage.getItem('submittedForms');
        let mergedForms: any[] = [];
  
        if (savedForms) {
          mergedForms = JSON.parse(savedForms);
        }
  
        // Append new form data to existing forms
        mergedForms.push(updatedFormData);
  
        // Update submittedForms in sessionStorage with merged data
        sessionStorage.setItem('submittedForms', JSON.stringify(mergedForms));
        window.location.href = `/dashboard/${user.email}`;
        
        handlePopup("");
        
         // Update the context with the new form data
      } else {
        console.error('Failed to submit form data');
      }
    } catch (error) {
      console.error('Error submitting form data:', error);
    } finally {
      setLoading(false);
      
    }
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          className="bg-white hover:bg-zinc-100 text-black"
          type="submit"
          onClick={() => handlePopup(isClicked)}
        >
          {title}
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px] bg-white">
        <DialogHeader>
          {isClicked === "joinClass" && <DialogTitle>Join Class</DialogTitle>}
          {isClicked === "createClass" && <DialogTitle>Create Class</DialogTitle>}
          {isClicked === "joinClass" && (
            <DialogDescription>
              Use a class code with 5-7 letters or numbers, and no spaces or
              symbols
            </DialogDescription>
          )}
          {isClicked === "createClass" && (
            <DialogDescription>
              Complete the form to create a class
            </DialogDescription>
          )}
        </DialogHeader>
        <div className="grid gap-4 py-4">
          {isClicked === "joinClass" && (
            <div className="flex gap-4 flex-col items-start">
              <Label htmlFor="classCode" className="text-right">
                Class Code
              </Label>
              <Input value={localFormData.classCode} onChange={handleInputChange} id="classCode" defaultValue="" className="col-span-3" />
            </div>
          )}
          {isClicked === "createClass" && (
            <div className="flex gap-4 flex-col items-start">
              <Label htmlFor="className" className="text-right">
                Class Name (required)
              </Label>
              <Input id="className" defaultValue="" className="col-span-3" value={localFormData.className} onChange={handleInputChange}/>
              <Label htmlFor="subject" className="text-right">
                Subject
              </Label>
              <Input id="subject" defaultValue="" className="col-span-3" value={localFormData.subject} onChange={handleInputChange}/>
              <Label htmlFor="section" className="text-right">
                Section
              </Label>
              <Input value={localFormData.section} onChange={handleInputChange} id="section" defaultValue="" className="col-span-3" />
            </div>
          )}
        </div>
        <DialogFooter>
          <Button
            className="drop-shadow-lg bg-[#37986A] hover:bg-[#2D7753] border border-green-400"
            type="submit"
            onClick={handleSubmit}
            disabled={loading}
          >
            {!loading ? (
              "Save changes"
            ) : (
              "Creating"
            )}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}

export default DialogComp;

