"use client"
import React, { useState } from "react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import Image from "next/image";
import plus from "@/assets/svgs/plus.svg"
import DialogComp from "@/components/DialogComp";
import { getAuth } from "firebase/auth";
import { app } from "@/app/firebaseConfig";

const DropDownComp = ({}) => {

  

    const [isClicked, setIsClicked] = useState("")
    function handlePopup(id : string) {
       setIsClicked(id)
       if(isClicked === "joinClass"){
        console.log("Join class clicked")
       }
       if(isClicked === "createClass"){
        console.log("create class clicked")
       }
    }

    const goToMainMenu = () => {
      const auth = getAuth(app);
      const user = auth.currentUser;
      if(user){
        window.location.href = `/dashboard/${user.email}`
      }
      else{
        window.location.href = `/login`
      }
    }
    return (
        
     
       
          <DropdownMenu>
  <DropdownMenuTrigger>
    <div className="hover:bg-zinc-100 rounded-full p-1">
    <Image className="bg-transparent" src={plus} alt="" width={25} height={25}/>
    </div>
    
  </DropdownMenuTrigger>
  <DropdownMenuContent>
    <DropdownMenuSeparator />
    <div className="flex flex-col ">
      <DialogComp
      handlePopup={handlePopup}
      isClicked="joinClass"
      title="Join Class"
      />
    
   
    
        <DialogComp
        handlePopup={handlePopup}
        isClicked="createClass"
        title="Create Class"
        />
     <DropdownMenuItem onClick={goToMainMenu} className="">
      <span className="text-center w-full cursor-pointer bg-transparent">Dashboard</span>
     </DropdownMenuItem>

      </div>
    
  </DropdownMenuContent>
</DropdownMenu>
       
       
    )

}
export default DropDownComp