
"use client";

import React, { useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { Button } from "@/components/ui/button";
import { Dialog } from "@headlessui/react";
import { usePathname } from "next/navigation";
import { useSelector, useDispatch } from "react-redux";
import logo from '@/assets/svgs/logo.svg'
import { makeItOpen, makeItClose, selectMobileMenuOpen } from "@/features/mobilemenu/mobileMenuSlice";

const DemoHeader = () => {
    const pathname = usePathname();
  const mobileMenuOpen = useSelector(selectMobileMenuOpen);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(makeItClose())
  }, [pathname])
  return (
  
    <header className="z-30 sticky md:p-2 lg:p-0 p-1 lg:px-20 max-w-screen top-0 mt-2">
      
      <nav className="bg-white px-4 lg:px-6 py-2 text-black">
    
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
          
          <div className="flex gap-20 items-center">
            <Link href="/" className="flex items-center gap-3">
              <Image src={logo} alt="logo" width={30} height={25}/>
              <span className="self-center text-xl font-medium whitespace-nowrap text-black">
                StudySphere
              </span>
            </Link>
            <div className="flex mt-4 font-semibold lg:space-x-6 lg:mt-0  lg:block hidden">
             
            <Button
              className="font-medium tracking-normal text-sm "
              variant="ghost"
              asChild
            >
              <Link href="/aboutus">About us</Link>
            </Button>
             
             
                <Button
              className="tracking-normal text-sm "
              variant="ghost"
              asChild
            >
              <Link href="/blog">Blog</Link>
            </Button>
             
            </div>
          </div>
          <div className="flex items-center lg:order-2 lg:block hidden space-x-10">
          {/* I made this project because I want to make the best project in the class */}
              
          <Button className="tracking-normal text-sm " variant="ghost" asChild>
            <Link href="/faqs">FAQs</Link>
          </Button>
             
            
                <Button className="tracking-normal w-20 h-10 drop-shadow-lg bg-[#37986A] hover:bg-[#2D7753] relative bottom-[2px] border border-green-400 text-[1rem]">
            <a className="bg-transparent text-[13px]" href="/login">Sign in</a>
          </Button>
          
          </div>
          <button
            type="button"
            className="-m-2 inline-flex items-center justify-center rounded-full p-2.5 hover:bg-zinc-100 transition duration-200 ease-in-out"
            onClick={() => dispatch(makeItOpen())}
          >
            <span className="sr-only">Open main menu</span>
            <Bars3Icon className="h-6 w-6  lg:hidden bg-transparent" aria-hidden="true" />
          </button>
        </div>
      </nav>

      <Dialog
        as="div"
        className="lg:hidden "
        open={mobileMenuOpen}
        onClose={() => dispatch(makeItClose())}
      >
        <div className="fixed inset-0 z-30 tracking-tight w-screen" />

        <Dialog.Panel className="text-black fixed inset-y-0 right-0 z-30 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-screen sm:ring-1 drop-shadow-lg animate-fade animate-duration-200 animate-ease-in-out transform-gpu">
          <div className="flex items-center justify-between bg-white ">
            <span className="tracking-normal font-normal  ">StudySphere</span>
            <button
              type="button"
              className="-m-2.5 p-2.5  rounded-full bg-white hover:bg-zinc-200 z-20"
              onClick={() => dispatch(makeItClose())}
            >
              <span className="sr-only">Close menu</span>
              <XMarkIcon className="h-6 w-6 rounded-full bg-transparent" aria-hidden="true" />
            </button>
          </div>
          <div className="mt-6 flow-root bg-[#232323] animate-fade-up animate-duration-500 animate-ease-in-out divide-y grid grid-cols-1 ">
            <div className="-my-6 divide-y divide-gray-500/10 white">
              <div className="space-y-4 py-6 tracking-normal bg-white">
                <Link
                  href="/aboutus"
                  className="bg-white -mx-3 block  px-3 py-2 text-base leading-7 hover:bg-zinc-100 transition-all ease-in-out tracking-normal"
                >
                  About Us
                </Link>
                <Link
                  href="/faqs"
                  className="bg-white -mx-3 block  px-3 py-2 text-base leading-7 hover:bg-zinc-100 transition-all ease-in-out tracking-normal "
                >
                  FAQs
                </Link>
                <Button className=" px-3 tracking-normal h-8 drop-shadow-lg bg-[#37986A] w-full hover:bg-[#2D7753] relative bottom-[2px] border border-green-400 text-[1rem]">
            <a className="bg-transparent text-[13px]" href="/login">Sign in</a>
          </Button>
              </div>

             
             
            </div>
          </div>
        </Dialog.Panel>
      </Dialog>
      <hr
     className="mx-auto md:mx-6 sm:mx-6 xs:mx-6 h-px border-t-0 bg-zinc-500 opacity-25 mt-2" />
    
     
    </header>
     
  );
};

export default DemoHeader;
