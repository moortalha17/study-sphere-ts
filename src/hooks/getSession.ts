// // hooks/useAuth.js
// import { useEffect, useState } from "react";
// import {User, getAuth, onAuthStateChanged } from "firebase/auth";
// import { useRouter } from "next/navigation";
// import app from "@/auth/providers/firebase";


// const useAuth = () => {
//     const [user, setUser] = useState<User | null>(null);
//     const [loading, setLoading] = useState(true);
//     const auth = getAuth(app);
//     const router = useRouter();

//     useEffect(() => {
//         const unsubscribe = onAuthStateChanged(auth, (user) => {
//             if (user) {
//                 setUser(user);
//                 window.location.href = `/optionspage/${user.displayName}`;
//             } else {
//                 setUser(null);
//             }
//             setLoading(false);
//         });

//         return () => unsubscribe();
//     }, [auth, router]);

//     return { user, loading };
// };

// export default useAuth;


