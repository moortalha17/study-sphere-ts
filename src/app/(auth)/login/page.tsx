"use client"


import LoginComp from '@/components/LoginComp'
import { store } from '@/store/store'
import React from 'react'
import { Provider } from 'react-redux'

const LoginPage = () => {
  return (
    <Provider store={store}>
  
         
      <LoginComp/>


    </Provider>
  )
}

export default LoginPage