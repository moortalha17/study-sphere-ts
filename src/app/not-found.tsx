import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components/ui/button'; // Assuming you're using Chakra UI for styling
import lost from '@/assets/svgs/lost.svg'

const NotFoundPage = () => {
  return (
    <div className="flex flex-col items-center justify-center h-screen">
      <Image src={lost} alt="404 Page" width={450} height={300} />
      <h1 className="text-2xl sm:text-3xl md:text-4xl text-center font-bold mt-8">
        Oops! Page not found
      </h1>
      <p className="text-lg sm:text-xl md:text-xl text-center text-zinc-600 mt-4">
        I think you are lost
      </p>
      <Button
        className="mt-4 sm:w-48 bg-white  text-black outline outline-1 outline-zinc-300 shadow-sm hover:bg-zinc-100"
      
      >
        <Link href="/" className='bg-transparent'>
         Return Home
        </Link>
      </Button>
    </div>
  );
};

export default NotFoundPage;
