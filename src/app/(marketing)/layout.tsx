"use client";
import React, { useEffect, useState } from "react";
import FooterComp from '@/components/FooterComp';
import DemoHeader from "@/components/DemoHeader";
import { Provider } from "react-redux";
import { store } from "@/store/store";
import { useRouter } from "next/navigation";
import {
  getAuth,
  onAuthStateChanged,
} from "firebase/auth";
import { app } from "@/app/firebaseConfig";
import LoaderComp from "@/components/LoaderComp";


const MarketingLayout = ({ children }: { children: React.ReactNode }) => {
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const auth = getAuth(app);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        router.push(`/dashboard/${user.email}`);
      } else {
        setLoading(false);
      }
    });

    // Cleanup subscription on unmount
    return () => unsubscribe();
  }, [auth, router]);

  if (loading) {
    return (
      <div className="flex h-dvh justify-center items-center"><LoaderComp/></div>
      
    );
  }

  return (
    <div>
      <meta name="theme-color" content="#fff" />
      <Provider store={store}>
        <DemoHeader />
        {children}
        <FooterComp />
      </Provider>
    </div>
  );
};

export default MarketingLayout;

