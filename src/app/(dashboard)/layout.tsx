"use client"
import React, {  ReactNode, createContext, useContext, useEffect, useState } from 'react';
import DropDownComp from "@/components/DropDownComp";
import logo from '@/assets/svgs/logo.svg'
import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components/ui/button';
import { getAuth } from 'firebase/auth';
import { app } from '@/app/firebaseConfig';
import { useRouter } from 'next/navigation';
import { redirect } from 'next/navigation';

const DashboardLayout =  ({
    children
} : {
    children : React.ReactNode
}) => {

  const auth = getAuth(app);
  

  const [user, setUser] = useState(auth.currentUser);

  
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      // Update user state when auth state changes
      setUser(user);
    });
    return () => unsubscribe();
  }, [auth]);


  const router = useRouter();

  // Function to handle navigation to the created page
  const handleNavigateToCreated = () => {
    router.push(`/dashboard/${user?.email}/created`);
  };

  // Function to handle navigation to the joined page
  const handleNavigateToJoined = () => {
    router.push(`/dashboard/${user?.email}/joined`);
  };

    return (
      
    <>
          <header className="max-w-screen">
            <nav className="mx-auto max-w-7xl p-6 flex drop-shadow-sm font-medium sticky z-10 justify-between">
              <div className='flex gap-5 items-center sm:gap-10'>
              <div className='flex items-center gap-3'>
            <Image src={logo} alt="logo" width={25} height={25}/>
              <span>StudySphere</span>
              </div>
              <div className='flex  items-center sm:gap-6'>
              <Button
              className="font-medium tracking-normal text-sm "
              variant="ghost"
              asChild
              onClick={handleNavigateToCreated}
            >
              <span className='cursor-pointer'>Created</span>
            </Button>
            <Button
              className="font-medium tracking-normal text-sm "
              variant="ghost"
              asChild
              onClick={handleNavigateToJoined} 
            >
              <span className='cursor-pointer'>Joined</span>
            </Button>
              </div>
              </div>
             <DropDownComp/>
              
              
            </nav>
            <hr className="mx-auto md:mx-6 sm:mx-6 xs:mx-6 h-px border-t-0 bg-zinc-500 opacity-25 " />
          </header>
          {children}
         </>
         
    );
}

export default DashboardLayout;

