import React from 'react'
import Image from 'next/image'
import grades from "@/assets/svgs/grades.svg"

const Marks = () => {
  return (
    <div className='flex flex-col max-w-screen items-center gap-20 sm:mt-32 '>
        <Image src={grades} alt='' width={250}/>
        <div>
            <p>This is where you'll view and manage grades</p>
           
        </div>
    </div>
  )
}

export default Marks