import { Button } from "@/components/ui/button";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import classwork from "@/assets/svgs/classwork.svg";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import DatePicker from "./DatePicker";
import UploadComponent from "./UploadComp";
import { app } from "@/app/firebaseConfig"; // Assuming you've imported storageRef correctly
import {
  UploadTask,
  getDownloadURL,
  getStorage,
  ref,
  uploadBytes,
  uploadBytesResumable,
} from "firebase/storage";
import { LucideUploadCloud, File } from "lucide-react";
import "../../../../../../custom/uploadComp.css";

import { useToast } from "@/components/ui/use-toast";
import { useParams } from "next/navigation";
import { Label } from "@/components/ui/label";
 
interface fetchFiles {
  fileName : string,
  fileSize :number
  
}


const Classwork = () => {
  const { className, classCode } = useParams();

  const { toast } = useToast();

  const storage = getStorage(app);
  const [file, setFile] = useState<File | null>(null);
  const [uploadProgress, setUploadProgress] = useState<number>(0); // Start with 0 progress
  const [uploadedFileName, setUploadedFileName] = useState<string>(""); // Empty string initially
  const [uploadedFileSize, setUploadedFileSize] = useState<string>("");
  const [checkMark, setCheckMark] = useState(false);
  const [disabled, setIsDisabled] = useState(false);
  const [processing, setProcessing] = useState(false);

  const [date, setDate] = React.useState<Date>();

 const [fetchFiles, setFetchFiles] = useState<fetchFiles[]>([])

  const [materialData, setMaterialData] = useState({
    title: "",
    date: new Date(),
    points: "",
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    setMaterialData((prevData) => ({
      ...prevData,
      [id]: value,
    }));
  };

  const handleDateChange = (day: Date | undefined) => {
    if (day instanceof Date) {
      // Handle the case when a valid date is selected
      setMaterialData((prevData) => ({
        ...prevData,
        date: day,
      }));
    }
  };

  const formatFileSize = (size: number): string => {
    if (size === 0) return "0 Bytes";
    const k = 1024;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    const i = Math.floor(Math.log(size) / Math.log(k));
    return parseFloat((size / Math.pow(k, i)).toFixed(2)) + " " + sizes[i];
  };

  const handleChange = (selectedFile: File) => {
    setUploadProgress(0);
    setCheckMark(false);
    setFile(selectedFile);
    setUploadedFileName(selectedFile.name);
    setUploadedFileSize(formatFileSize(selectedFile.size));
  };

  const handleUpload = () => {
    console.log(materialData);
    if (!file) return;
    setIsDisabled(true);
    setProcessing(true);

    const storageRef = ref(storage, `files/${file.name}`);
    const uploadTask: UploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setUploadProgress(progress);
        setProcessing(false);
        console.log("Upload progress:", progress);
      },
      (error) => {
        console.error("Error uploading file:", error);
      },
      async () => {
        try {
          const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
          console.log("File uploaded successfully. Download URL:", downloadURL);
          setUploadedFileName(file.name);

          await fetch("http://localhost:8080/api/storeFileMetaData", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              className,
              classCode,
              materialData,
              fileName: file.name,
              fileSize: file.size,
              downloadURL,
            }),
          });
        } catch (error) {
          console.error("Error storing file metadata:", error);
        } finally {
          setCheckMark(true);
          toast({
            title: "File Uploaded Successfully",
          });
          setIsDisabled(false);
        }
      }
    );
  };

  useEffect(() => {
    const fetchFiles = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/fetchFiles/${classCode}`);
        if(!response.ok){
          throw new Error("Response was not okay")
        }

        const data = await response.json()
        setFetchFiles(data)
        sessionStorage.setItem('files from cloud', JSON.stringify(date))
      }
      catch(error :any) {
        console.error("Error fetching files", error)
      }

    }

    fetchFiles()

  },[classCode])

  return (
    <div className="flex flex-col max-w-screen items-center gap-20 ">
      <div className="w-full max-w-md">
        <Dialog>
          <DialogTrigger asChild>
            <Button className="rounded-full px-7 py-4 h-13 shadow-xl w-full max-w-24 place-self-start">
              <span className="bg-transparent text-white text-[1rem] ">
                Create
              </span>
            </Button>
          </DialogTrigger>
          <DialogContent className="bg-white max-w-screen h-svh">
            <DialogHeader className="text-start text-3xl">
              Create
              <DialogDescription className="text-sm">
                Add assignments, materials, or questions
              </DialogDescription>
            </DialogHeader>
            <div className="flex flex-col justify-start max-w-screen items-center ">
              <div className="space-y-1 w-full max-w-lg mb-10">
                <Label htmlFor="title">Title</Label>
                <Input
                  id="title"
                  defaultValue=""
                  className="outline-1 outline-zinc-400 ring-offset-background placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50 text-5xl h-18"
                  value={materialData.title}
                  onChange={handleInputChange}
                />
              </div>
              <UploadComponent
                file={file}
                uploadProgress={uploadProgress}
                uploadedFileName={uploadedFileName}
                uploadedFileSize={uploadedFileSize}
                checkMark={checkMark}
                handleChange={handleChange}
                disabled={disabled}
                processing={processing}
              />
              <div className="space-y-4 w-full max-w-lg mt-10">
                <div className="flex items-center gap-7">
                  <div>
                <Label htmlFor="title">Pick Due Date</Label>
                  <DatePicker
                    date={materialData.date}
                    handleDateChange={handleDateChange}
                  />
                  </div>
                  <div>
                  <Label htmlFor="title">Points</Label>
                  <Input
                    id="points"
                    defaultValue=""
                    className="outline-1 outline-zinc-400 ring-offset-background placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50 max-w-[280px] w-full text-lg"
                  
                    value={materialData.points}
                    onChange={handleInputChange}
                  />
                  </div>
                </div>
                <Button
                  className="drop-shadow-lg bg-[#37986A] hover:bg-[#2D7753] border border-green-400 w-full h-12"
                  type="submit"
                  onClick={handleUpload}
                  disabled={disabled}
                >
                  {uploadProgress === 0 && disabled ? "Processing" : "Create"}
                </Button>
              </div>
            </div>

            <DialogFooter></DialogFooter>
          </DialogContent>
        </Dialog>
      </div>
      <div className="text-center">
        <Image src={classwork} alt="" width={250} />
        <div className="mt-10">
          <p>This is where you'll assign work</p>
          <p className="text-zinc-500">
            You can add assignments and other work for the class
          </p>
        </div>
      </div>
      <div className="max-w-md w-full flex flex-col items-center mt-10">

       {fetchFiles.map((file, index) => (
        <div className="w-full max-w-md">
        <hr className="h-px border-t-0 bg-zinc-500 opacity-25 max-w-md w-full" />
        <div className="w-full flex justify-between items-center">
          <div className="w-full max-w-md pt-4 pb-4 flex gap-2">
            <File size={38} color="#37986A" />
            <div className="flex flex-col text-sm">
              <span>{file.fileName}</span>
              <span className="text-zinc-400 text-xs ">{formatFileSize((file.fileSize))}</span>
            </div>
          </div>

        </div>
      </div>

       ))} 
      </div>
    </div>
  );
};

export default Classwork;
