import { LucideUploadCloud, File } from 'lucide-react';
import "../../../../../../custom/uploadComp.css"
import { Progress } from "@/components/ui/progress"
import CheckMark from './CheckMark';

interface UploadComponentProps {
  handleChange : (file : File) =>  void
  uploadedFileName : string,
  uploadedFileSize : string,
  uploadProgress : number,
  checkMark : boolean,
  file : File | null
  disabled:boolean
  processing : boolean
}

const UploadComponent = ({file, uploadedFileName, uploadedFileSize, uploadProgress, checkMark, handleChange, disabled, processing
}:UploadComponentProps) => {

  const onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files && files.length > 0) {
      handleChange(files[0]);
    }
  };


  return (
    <div className='max-w-md flex flex-col h-full w-full cursor-pointer file-input-div '>
    {/* Label to trigger file input */}
    <label htmlFor="fileInput" className='w-full flex flex-col  rounded pb-4 items-center justify-center h-full pt-4 cursor-pointer outline outline-1 outline-zinc-400'>
      <LucideUploadCloud color='#6B7280' />
      <span className='text-zinc-500 text-sm'>Upload Files</span>
      <input id="fileInput" type="file" className="hidden" onChange={onFileChange} />
    </label>

    {uploadedFileName && (
      <div className='flex items-start w-full gap-1 px-3 mt-5 justify-between'>
        <div className='flex gap-1'>
        <File size={38} color='#6B7280' />
        <div className='flex flex-col'>
        <span className='text-zinc-500 text-sm'>{uploadedFileName}</span>
        
        <span className='text-zinc-400 text-xs'>{uploadedFileSize}</span>
        </div>
        </div>
        {checkMark && <div className=''>
          <CheckMark />
          
          </div>}
      </div>
    )}

    {uploadProgress > 0 && <Progress className='mt-7' value={uploadProgress} max={100} />

    }
    {uploadProgress === 100 &&
    !checkMark && <span className='text-zinc-400 text-sm text-end mt-1'>Finishing up</span>  }

    {
    processing &&  <span className='text-zinc-400 text-sm text-end mt-1'>Processing</span>  }
    

  
  </div>
  );
};

export default UploadComponent;


