import Image from 'next/image'
import React, { Suspense, useEffect, useState } from 'react'
import studentClasswork from '@/assets/svgs/studentClasswork.svg'
import "../../../../../../custom/boxshadow.css";
import { File, Download} from 'lucide-react';
import Loading from '../../../loading';
interface StudentClassworkProps {
    classCode : string,
    
    
}
interface FilesData {
  fileName : string,
  timestamp :string,
  downloadURL : string,
}

interface DownloadUrl {
  downloadUrl : string
}

const StudentClassworks: React.FC<StudentClassworkProps> = ({classCode}) => {

  const [fetchFiles, setFetchFiles] = useState<FilesData[]>([])
  const [imageLoading, setImageLoading] = useState(true);

  const handleDownload = (url: string) => {
    if (!url) {
      console.error('Download URL is invalid');
      return;
    }

    console.log('Downloading file from URL:', url);

    const link = document.createElement('a');
    link.href = url;
    link.target = '_blank';
    const fileName = url.split('/').pop();
    link.download = fileName || 'download';
    console.log('File name:', fileName);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };
  useEffect(() => {
    const fetchFiles = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/fetchFiles/${classCode}`);
        if(!response.ok){
          throw new Error("Response was not okay")
        }

        const data = await response.json()
        setFetchFiles(data)
        sessionStorage.setItem('files from cloud', JSON.stringify(data))
      }
      catch(error :any) {
        console.error("Error fetching files", error)
      }

    }

    fetchFiles()
    
  },[classCode])

console.log(fetchFiles)
  
  return (
    <div className='flex flex-col h-svh max-w-screen items-center'>
      <div className='w-full max-w-2xl mt-10 '>
        <div className='w-full flex justify-end'>
        
        
          <Image  src={studentClasswork} className='' alt='' width={350}/>
          
          </div>
        <div className=' z-10 relative bottom-36 h-screen announcementDiv '><span className='text-zinc-800 text-4xl font-bold relative top-7 left-7 underline'>Work Assigned</span>
          
          <div className='mt-16'>

       
         {fetchFiles.map((file, index) => ( <div key={index} className="w-full max-w-2xl  px-7 pt-5">
            <hr className="h-px border-t-0 bg-zinc-500 opacity-25 " />
            <div className="w-full max-w-2xl pt-4 pb-4 flex place-self-center justify-between">
              <div className='flex gap-2'>
              <File size={38}  color="#37986A"/>
              <p className="text-sm">{file.fileName}</p>
              </div>
              <div className=''><Download  size={38} onClick={() => handleDownload(file.downloadURL)} color="#37986A" className='cursor-pointer'/></div>
           
            </div>
      
          </div>))}
        
        

          </div>
        </div>
      </div>
    </div>
  )
}

export default StudentClassworks