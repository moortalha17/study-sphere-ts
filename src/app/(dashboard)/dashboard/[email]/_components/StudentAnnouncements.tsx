import Image from 'next/image'
import React, { useEffect, useState } from 'react'
import studentAnnounc from '@/assets/svgs/studentAnnounc.svg'
import "../../../../../../custom/boxshadow.css";

interface StudentAnnouncementsProps {
    classCode : string
}

interface StudentAnnouncements {
  classCode: string;
  content: string;
  timestamp: string;
}

 

 
const StudentAnnouncements: React.FC<StudentAnnouncementsProps> = ({ classCode}) => {

  const [announcements, setAnnouncements] = useState<StudentAnnouncements[]>([]);
  const [showLoader, setShowLoader] = useState(true);

  useEffect(() => {
    const fetchAnnouncements = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/getAnnouncements/${classCode}`);
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        
        const data = await response.json();
        
        
        setAnnouncements(data);
        sessionStorage.setItem('posts', JSON.stringify(data));
      } catch (error) {
        console.error('Error fetching announcements:', error);
      }
      finally{
        setShowLoader(false)
      }
      
      console.log("fetched from firebase storage")
      
    };
   

    fetchAnnouncements();
  }, [classCode]);



  return (
    <div className='flex flex-col h-svh max-w-screen items-center'>
      <div className='w-full max-w-2xl '>
        <div><Image src={studentAnnounc} alt='' width={390}/></div>
        <div className=' z-10 relative bottom-32 h-full announcementDiv '><span className='text-zinc-800 text-4xl font-bold relative top-7 left-7 underline'>Important Announcements</span>
     
       <div className='mt-16'>

          {announcements.map((announcement , index) => (<div key={index} className="w-full max-w-2xl px-7 pt-7">
            <hr className="h-px border-t-0 bg-zinc-500 opacity-25 " />
            <div className="w-full max-w-2xl pt-4 pb-4 flex flex-col place-self-center "><p className="">{announcement.content}</p>
            <span className="text-zinc-400 text-xs text-end mt-4">{announcement.timestamp}</span>
            </div>
          </div>))}
         
         
          </div>
        
        
       
       
        </div>
      </div>
    </div>
  )
}

export default StudentAnnouncements