import React, { useEffect, useState } from "react";
import "../../../../../../custom/boxshadow.css";
import Annon from "@/assets/svgs/announcement.svg";
import Image from "next/image";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Textarea } from "@/components/ui/textarea";



interface Announcement {
  id: string;
  classCode: string;
  content: string;
  timestamp: string;
}

interface FirestoreAnnouncement {
  id: string;
  classCode: string;
  content: string;
  timestamp: string
}

interface AnnouncementProps {
  className: string;
  classCode: string;
}

const Announcement: React.FC<AnnouncementProps> = ({
  className,
  classCode,
}) => {

  const [content, setContent] = useState('');
  const [announcements, setAnnouncements] = useState<Announcement[]>([]);
  const [loading, setLoading] = useState(false);
  const [showLoader, setShowLoader] = useState(true);

 

  
  useEffect(() => {
    const fetchAnnouncements = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/getAnnouncements/${classCode}`);
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        
        const data = await response.json();
        
        
        setAnnouncements(data);
        sessionStorage.setItem('posts', JSON.stringify(data));
      } catch (error) {
        console.error('Error fetching announcements:', error);
      }
      finally{
        setShowLoader(false)
      }
      
      console.log("fetched from firebase storage")
      
    };
   

    fetchAnnouncements();
  }, [classCode]);



  // useEffect(() => {
  //   const savedAnnouncements = sessionStorage.getItem( 'posts')
  //   console.log("fetched from session storage")
  //   if(savedAnnouncements){
  //     const mergedAnnoun = JSON.parse(savedAnnouncements)
  //     setAnnouncements(mergedAnnoun)
  //     setShowLoader(false);
  //   }
  // },[])

  // const formatDate = (date: Date): string => {
  //   return new Intl.DateTimeFormat(navigator.language, {
  //     day: '2-digit',
  //     month: 'short',
  //     year: 'numeric',
  //     hour: '2-digit',
  //     minute: '2-digit',
  //     second: '2-digit',
  //     timeZoneName: 'short'
  //   }).format(date);
  // };

  const handlePost = async () => {
    setLoading(true)
    if (content.trim()) {
      try {
        const response = await fetch('http://localhost:8080/api/postAnnouncements', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ classCode, content }),
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        const fetchedAnnouncements = await fetch(`http://localhost:8080/api/getAnnouncements/${classCode}`);
        const announcementsData = await fetchedAnnouncements.json();
        setAnnouncements(announcementsData);
        sessionStorage.setItem('posts', JSON.stringify(announcementsData));
        setContent('');
      } catch (error) {
        console.error('Error posting announcement:', error);
      }
      finally{
        setLoading(false)
      }
    }
  };

 
  
  return (
    <>
    <div className="flex flex-col gap-4 max-w-screen items-center mx-auto lg:flex-row lg:items-center lg:justify-center lg:space-x-10 sm:mt-32">
      <div className="w-full max-w-md space-y-5">
        <div className="flex flex-col gap-3  text-start ">
          <span className="text-5xl">{className}</span>
          <span>{classCode}</span>
        </div>
        <Dialog>
          <DialogTrigger asChild>
            <div className="w-full max-w-md">
              <div className="announcementDiv rounded cursor-pointer hover:bg-zinc-100 transition duration-200 ">
                <span className="text-zinc-400 bg-transparent">
                  Announce something to your class
                </span>
              </div>
            </div>
          </DialogTrigger>
          <DialogContent className="sm:max-w-[425px] bg-white">
            <DialogHeader>Announce</DialogHeader>
            <DialogDescription>Make announcement for your students</DialogDescription>
            <Textarea value={content} onChange={(e) => setContent(e.target.value)} />
            <DialogFooter>
              <Button
                className="drop-shadow-lg bg-[#37986A] hover:bg-[#2D7753] border border-green-400 w-24"
                type="submit"
                onClick={handlePost}
                disabled={loading}
              >
                 {!loading ? (
              "Post"
            ) : (
              "Posting"
            )}
              </Button>
            </DialogFooter>
          </DialogContent>
        </Dialog>
      </div>
      <Image src={Annon} alt="" width={250} className="mt-10" />
    
    </div>
   
   <div className="max-w-screen flex flex-col items-center mt-14">
 
    {announcements.map((announcement, index) => (
          <div key={index} className="w-full max-w-md">
            <hr className="h-px border-t-0 bg-zinc-500 opacity-25 max-w-md w-full" />
            <div className="w-full max-w-md pt-4 pb-4 flex flex-col place-self-center "><p className="">{announcement.content}</p>
            <span className="text-zinc-400 text-xs text-end mt-4">{announcement.timestamp.toString()}</span>
            </div>
          </div>
        ))}
       
   
   
    </div>
    
      </>
  );
};

export default Announcement;
