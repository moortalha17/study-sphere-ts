import { app } from "@/app/firebaseConfig";
import { Button } from "@/components/ui/button";
import { getAuth } from "firebase/auth";
import Link from "next/link";
import { useParams } from "next/navigation";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";

const Stream = () => {
  const params = useParams() as {
    className: string | string[];
    classCode: string | string[];
  };
 
  const className = Array.isArray(params.className) ? params.className[0] : params.className;
  const classCode = Array.isArray(params.classCode) ? params.classCode[0] : params.classCode;
  const router = useRouter()
  const [meetingId, setMeetingId] = useState("");
  const auth = getAuth(app);
  const user = auth.currentUser;

  const handleStartMeeting = async () => {
    try {
      // Call Dyte API to create meeting
      const dyteResponse = await fetch('https://api.dyte.io/v2/meetings', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic OTMwZTlkODMtOTdhNy00MjAyLTg2ZTYtNzhlYzA4OGEzYjlmOjMwMjIxNTZiNWU2YjJjNjdjNTkz', // Replace with your Dyte API key
        },
        body: JSON.stringify({
          "title": "My Meeting",
          "preferred_region": "ap-south-1",
          "record_on_start": false,
          "live_stream_on_start": false,
        }),
      });

      const dyteData = await dyteResponse.json();
      const newMeetingId = dyteData.data.id;
      
      // Store meeting ID in your Express API
      const uid = user?.uid
      const expressResponse = await fetch('http://localhost:8080/api/storeMeetingId', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ uid, meetingId: newMeetingId }),
      });

      if (!expressResponse.ok) {
        throw new Error('Failed to store meeting ID');
      }

      setMeetingId(newMeetingId);
      router.push(`/dashboard/${user?.email}/created/${className}/${classCode}/${meetingId}`)
    } catch (error) {
      console.error('Error starting meeting:', error);
    }
  };

  // useEffect(() => {
  //   const url = 'https://api.dyte.io/v2/presets';
  //   const options = {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Accept: 'application/json',
  //       Authorization: 'Basic OTMwZTlkODMtOTdhNy00MjAyLTg2ZTYtNzhlYzA4OGEzYjlmOjMwMjIxNTZiNWU2YjJjNjdjNTkz'
  //     },
  //     body: '{"name":"talhasbox","config":{"view_type":"GROUP_CALL","max_video_streams":{"mobile":0,"desktop":0},"max_screenshare_count":0,"media":{"video":{"quality":"hd","frame_rate":30},"screenshare":{"quality":"hd","frame_rate":0}}},"permissions":{"accept_waiting_requests":true,"can_accept_production_requests":true,"can_edit_display_name":true,"can_spotlight":true,"is_recorder":false,"recorder_type":"NONE","disable_participant_audio":true,"disable_participant_screensharing":true,"disable_participant_video":true,"kick_participant":true,"pin_participant":true,"can_record":true,"can_livestream":true,"waiting_room_type":"SKIP","plugins":{"can_close":true,"can_start":true,"can_edit_config":true,"config":"4de5a1b6-f730-4513-9ebe-16895971a32d"},"connected_meetings":{"can_alter_connected_meetings":true,"can_switch_connected_meetings":true,"can_switch_to_parent_meeting":true},"polls":{"can_create":true,"can_vote":true,"can_view":true},"media":{"video":{"can_produce":"ALLOWED"},"audio":{"can_produce":"ALLOWED"},"screenshare":{"can_produce":"ALLOWED"}},"chat":{"public":{"can_send":true,"text":true,"files":true},"private":{"can_send":true,"can_receive":true,"text":true,"files":true}},"hidden_participant":true,"show_participant_list":true,"can_change_participant_permissions":true},"ui":{"design_tokens":{"border_radius":"rounded","border_width":"thin","spacing_base":4,"theme":"dark","colors":{"brand":{"300":"#844d1c","400":"#9d5b22","500":"#b56927","600":"#d37c30","700":"#d9904f"},"background":{"600":"#222222","700":"#1f1f1f","800":"#1b1b1b","900":"#181818","1000":"#141414"},"danger":"#FF2D2D","text":"#EEEEEE","text_on_brand":"#EEEEEE","success":"#62A504","video_bg":"#191919","warning":"#FFCD07"},"logo":"string"},"config_diff":{}}}'
  //   };

  //   const fetchData = async () => {
  //     try {
  //       const response = await fetch(url, options);
  //       const data = await response.json();
  //       console.log(data);
  //     } catch (error) {
  //       console.error('Error creating preset:', error);
  //     }
  //   };

  //   fetchData();
  // }, []); // Empty dependencies array to run this effect only once after component mount

  
 

 

  return (
    <div>
     
        <Button onClick={handleStartMeeting}>Start Meeting</Button>

    </div>
  );
};

export default Stream;
