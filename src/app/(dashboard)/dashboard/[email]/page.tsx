"use client"
import Image from 'next/image'
import React, { useEffect } from 'react'
import workspace from '@/assets/svgs/workspace.svg'
import { toast } from '@/components/ui/use-toast'
const DashboardPage = () => {

  useEffect(() => {
   toast({
    title: "Navigate between tabs to see your classes"
   }) 
   
  }, [])
  

        
  return (
    <div className='flex h-svh max-w-screen mt-10 justify-center'>
      <div className='px-10 flex flex-col sm:flex-row h-[23rem] w-full sm:justify-center sm:max-w-md sm:px-0 sm:gap-10 items-center'>
      <div className=' text-5xl font-bold tracking-tight  sm:place-self-center place-self-start'>
        <span className='text-zinc-800 sm:mt-10 '>Your<br/> Workspace </span></div>
      <div className=''><Image src={workspace} alt='' width={350} className='max-w-[20rem]'/></div>
      </div>
    </div>
  )
}

export default DashboardPage