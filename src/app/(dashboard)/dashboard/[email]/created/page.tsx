"use client";

import React, { useEffect, useState } from "react";
import { getAuth } from "firebase/auth";
import { app } from "@/app/firebaseConfig";
import LoaderComp from "@/components/LoaderComp";
import Link from "next/link";

interface FormDataType {
  classCode: string;
  className: string;
  subject: string;
  section: string;
  isCreated: boolean;
  isJoined: boolean;
}

const CreatedClassesPage = () => {
  const [submittedForms, setSubmittedForms] = useState<FormDataType[]>([]);
  const [showLoader, setShowLoader] = useState(true);

  useEffect(() => {
    const fetchAndSubmitForms = async () => {
      const auth = getAuth(app);
      const user = auth.currentUser;
      if (!user) {
        console.error("User is not authenticated");
        return;
      }

      const token = await user.getIdToken();

      try {
        // Fetch existing created classes
        const createdResponse = await fetch(
          "http://localhost:8080/api/fetchClass",
          {
            method: "GET",
            headers: {
              authorization: `Bearer ${token}`,
            },
          }
        );

        if (!createdResponse.ok) {
          throw new Error("Failed to fetch created classes");
        }

        const createdData = await createdResponse.json();
        const createdForms = createdData.classes.filter(
          (form: any) =>
            form.classCode && form.className && form.subject && form.section
        );

        // Fetch existing joined classe
        const forms = [...createdForms];
        setSubmittedForms(forms);
        // Store forms in sessionStorage
        sessionStorage.setItem("submittedForms", JSON.stringify(forms));
      } catch (error) {
        console.error("Error fetching or submitting forms:", error);
      } finally {
        setShowLoader(false);
      }
    };

    fetchAndSubmitForms();
  }, []);

  useEffect(() => {
    // Retrieve forms from sessionStorage on component mount
    const savedForms = sessionStorage.getItem("submittedForms");

    if (savedForms) {
      const mergedForms = JSON.parse(savedForms);
      setSubmittedForms(mergedForms);
      setShowLoader(false);
    }
  }, []);

  return (
    <div className="h-screen bg-white max-w-screen p-6 mx-auto px-[1.5rem] ">
      <div className="flex-start w-full justify-center gap-5 lg:px-10">
        <h1 className="text-4xl text-zinc-600 font-bold tracking-tight sm:text-5xl">Created Classes</h1>
      </div>

      <div className="w-full mx-auto p-2 sm:p-3 md:p-2 mt-9 lg:px-10">
        {showLoader && (
          <div className="flex justify-center items-center mt-10">
            <LoaderComp />
          </div>
        )}
        <div className="grid grid-cols-1 md:grid-cols-3 sm:grid-cols-2 gap-9 bg-transparent 2xl:grid-cols-4">
          {submittedForms.map((form, index) => (
            <Link
              href={`created/${form.className}/${form.classCode}`}
              key={index}
            >
              <div className="rounded overflow-hidden gap-3 flex flex-col outline outline-2 outline-zinc-300 cursor-pointer transition-all ease-in-out duration-200 bg-zinc-100 hover:bg-zinc-200 p-4">
                <span className="bg-transparent text-zinc-600 font-bold text-5xl">
                  {form.className}
                </span>
                <span className="bg-transparent text-zinc-800">
                  Subject: {form.subject}
                </span>
                <span className="bg-transparent text-zinc-800">
                  Section: {form.section}
                </span>
                <span className="bg-transparent text-zinc-500 italic">
                  Status: Created
                </span>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CreatedClassesPage;
