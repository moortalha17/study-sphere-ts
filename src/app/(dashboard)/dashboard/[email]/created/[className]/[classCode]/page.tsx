"use client"

import { Button } from "@/components/ui/button";
import React, { ReactNode, useEffect, useRef, useState } from "react";
import Announcement from "../../../_components/Announcement";
import Marks from "../../../_components/Marks";
import Classwork from "../../../_components/Classwork";
import { useParams } from "next/navigation";
import {app} from '@/app/firebaseConfig'
import { collection, getFirestore, query,where,getDocs } from "firebase/firestore";
import LoaderComp from "@/components/LoaderComp";
import '../../../../../../../../custom/activeBtn.css'
import { getAuth } from "firebase/auth";
import Stream from "../../../_components/Stream";


const ClassRoomPage = () => {

  
  const db = getFirestore(app)
  const auth = getAuth(app);
  const user = auth.currentUser;

  const params = useParams() as { className: string | string[], classCode: string | string[] };
  console.log(" Params are " , params)
  const className = Array.isArray(params.className) ? params.className[0] : params.className;
  const classCode = Array.isArray(params.classCode) ? params.classCode[0] : params.classCode;
  const [activeComponent, setActiveComponent] = useState('announcement');
  const [isValidParams, setIsValidParams] = useState(false);
  const [isLoading, setIsLoading] = useState(true); 

  useEffect(() => {
    // Check if className and classCode exist in Firestore
    const checkParams = async () => {
      try{
      const classesRef = collection(db, 'createdClasses');
      const q = query(classesRef, where('className', '==', className), where('classCode', '==', classCode));
      const snapshot = await getDocs(q);
      setIsValidParams(!snapshot.empty); 
      }
      catch(error){
        console.log("Error " , error )
      }// Set isValidParams based on query result
      finally {
        setIsLoading(false); // Set loading state to false after fetching data
      }
    };
  
    checkParams();
  }, [className, classCode]);

  const buttonRefs = {
    announcement: useRef<HTMLButtonElement | null>(null),
    classwork: useRef<HTMLButtonElement | null>(null),
    marks: useRef<HTMLButtonElement | null>(null),
    stream: useRef<HTMLButtonElement | null>(null),
  };
  type ComponentKey = 'announcement' | 'classwork' | 'marks' | 'stream'; 

  const handleClick = (component: ComponentKey) => {
    setActiveComponent(component);
     // Set focus on the clicked button
  };

  
  const renderComponent = () => {
    switch (activeComponent) {
      case 'announcement':
        return <Announcement className={className} classCode={classCode}/>;
      case 'classwork':
        return <Classwork />;
      case 'marks':
        return <Marks />;
        case 'stream':
        return <Stream/>;
      default:
        return null;
    }
  };

  if (isLoading) {
    return <div><LoaderComp/></div>; // Render loader while data is being fetched
  }

  if (!isValidParams) {
    return (
      <div className="max-w-screen h-svh flex items-center justify-center">
        <span className="text-5xl text-center">Class you are looking for
          <br/> doesnot exist</span>
      </div>
    )
  }

  return (
    <>

    <div  className="h-screen bg-white max-w-screen p-6 mx-auto px-[1.5rem]" >
      {" "}
      <div className="flex items-center w-full justify-center gap-5">
        <Button  ref={buttonRefs.announcement}
        onClick={() => handleClick("announcement")} className={`tracking-normal w-22 relative h-8 bg-white  outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white  ${activeComponent === 'announcement' ? 'active-btn' : ''}`}>
        Announcement

        </Button>
        <Button  ref={buttonRefs.classwork}
        onClick={() => handleClick("classwork")} className={`tracking-normal w-22 relative h-8 bg-white  outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white  ${activeComponent === 'classwork' ? 'active-btn' : ''}`}>
          Classwork
        </Button>
        <Button ref={buttonRefs.marks}
        onClick={() => handleClick("marks")} className={`tracking-normal w-22 relative h-8 bg-white   outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white ${activeComponent === 'marks' ? 'active-btn' : ''}`}>
         Marks
        </Button>
        <Button ref={buttonRefs.stream}
        onClick={() => handleClick("stream")} className={`tracking-normal w-22 relative h-8 bg-white   outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white ${activeComponent === 'marks' ? 'active-btn' : ''}`}>
         Stream
        </Button>
        
      </div>
      <div className="mt-10 pb-10">
        {renderComponent()}
      </div>
    </div>
    </>
  );
};

export default ClassRoomPage;
