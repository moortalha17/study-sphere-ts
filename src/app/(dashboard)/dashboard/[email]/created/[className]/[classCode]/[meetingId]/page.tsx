"use client"

import { useEffect, useState } from 'react';
import { DyteProvider, useDyteClient } from '@dytesdk/react-web-core';
import MyMeetingUI from '../../../../_components/MyMeetingUI';
import { useParams } from 'next/navigation';

export default function App() {
  const params = useParams() as { meetingId : string}; // Extract lassCode from params
const id = params.meetingId;
  const [token, setToken] = useState("");
  const [tokenFetched, setTokenFetched] = useState(false);
  const [meeting, initMeeting] = useDyteClient();

  

  useEffect(() => {
    const url = `https://api.dyte.io/v2/meetings/${id}/participants`;
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: 'Basic OTMwZTlkODMtOTdhNy00MjAyLTg2ZTYtNzhlYzA4OGEzYjlmOjMwMjIxNTZiNWU2YjJjNjdjNTkz'
      },
      body: JSON.stringify({
        name: 'Talha Mushtaq',
        preset_name: 'group_call_host',
        custom_participant_id: 'string'
      })
    };

    const fetchData = async () => {
      try {
        const response = await fetch(url, options);
        const data = await response.json();
        const newToken = data.data.token;
        setToken(newToken);
        setTokenFetched(true);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [id]);

  // const sendTokenToEnrolledUsers = async (token : string) => {
  //   try {
  //     const response = await fetch('http://localhost:8080/api/sendToken', {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify({
  //         token: token,
  //         classId: classCode, // Replace with the class ID
  //       }),
  //     });
  
  //     const data = await response.json();
  //     console.log(data); // Handle the response from the backend
  //   } catch (error) {
  //     console.error('Error sending token to enrolled users:', error);
  //   }
  // };
  

  useEffect(() => {
    if (tokenFetched) {
      initMeeting({
        authToken: token,
        defaults: {
          audio: false,
          video: false,
        },
      });
    }
  }, [token]);

  return (
    <div style={{ height: '100vh', width: '100vw' }}>
      <DyteProvider value={meeting}>
        <MyMeetingUI />
      </DyteProvider>
    </div>
  );
}
