"use client";

import React, { useEffect, useState } from "react";
import { getAuth } from "firebase/auth";
import { app } from "@/app/firebaseConfig";
import LoaderComp from "@/components/LoaderComp";
import Link from "next/link";

interface FormDataType {
  classCode: string;
  className: string;
 
}

const JoinedClassesPage = () => {
  const [submittedForms, setSubmittedForms] = useState<FormDataType[]>([]);
  const [showLoader, setShowLoader] = useState(true);

  useEffect(() => {
    const fetchJoinedClasses = async () => {
      const auth = getAuth(app);
      const user = auth.currentUser;
      if (!user) {
        console.error("User is not authenticated");
        return;
      }

      const token = await user.getIdToken();

      try {
        // Fetch existing created classes
        const joinedClassesResponse = await fetch(
          "http://localhost:8080/api/getJoinedClasses",
          {
            method: "GET",
            headers: {
              authorization: `Bearer ${token}`,
            },
          }
        );

        if (!joinedClassesResponse.ok) {
          throw new Error("Failed to fetch created classes");
        }

        const joinedClassesData = await joinedClassesResponse.json();
        const joinedClassesCards = joinedClassesData.classes.filter(
          (form: any) =>
            form.classCode && form.className 
        );

        // Fetch existing joined classe
        const cards = [...joinedClassesCards];
        setSubmittedForms(cards);
        // Store forms in sessionStorage
        sessionStorage.setItem("joinedClassesCards", JSON.stringify(cards));
      } catch (error) {
        console.error("Error fetching or submitting forms:", error);
      } finally {
        console.log("Fetched from firebase")
        setShowLoader(false);
      }
    };

    fetchJoinedClasses();
  }, []);

  useEffect(() => {
    // Retrieve forms from sessionStorage on component mount
    const joinedClassesCards = sessionStorage.getItem("joinedClassesCards");

    if (joinedClassesCards) {
      const mergedCards = JSON.parse(joinedClassesCards);
      setSubmittedForms(mergedCards);
      setShowLoader(false);
      console.log("Fetched from session storage")
    }
  }, []);

  return (
    <div className="h-screen bg-white max-w-screen p-6 mx-auto px-[1.5rem] ">
      <div className="flex-start w-full justify-center gap-5 lg:px-10">
        <h1 className="text-4xl text-zinc-600 font-bold tracking-tight sm:text-5xl">Joined Classes</h1>
      </div>

      <div className="w-full mx-auto p-2 sm:p-3 md:p-2 mt-9 lg:px-10">
        {showLoader && (
          <div className="flex justify-center items-center mt-10">
            <LoaderComp />
          </div>
        )}
        <div className="grid grid-cols-1 md:grid-cols-3 sm:grid-cols-2 gap-9 bg-transparent 2xl:grid-cols-4">
          {submittedForms.map((form, index) => (
            <Link
              href={`joined/${form.classCode}`}
              key={index}
            >
              <div className="rounded overflow-hidden gap-3 flex flex-col outline outline-2 outline-zinc-300  cursor-pointer transition-all ease-in-out duration-200 bg-zinc-100 hover:bg-zinc-200 p-4">
                <span className="bg-transparent  text-4xl text-zinc-600 font-bold">
                  Code: {form.classCode}
                </span>
                <span className="bg-transparent ">
                  Teacher: {form.className}
                </span>
                <span className="bg-transparent text-zinc-500 italic">
                  Status: Joined
                </span>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default JoinedClassesPage;
