"use client"

import { Button } from "@/components/ui/button";
import React, { ReactNode, Suspense, useEffect, useRef, useState } from "react";
import { useParams } from "next/navigation";
import {app} from '@/app/firebaseConfig'
import { collection, getFirestore, query,where,getDocs } from "firebase/firestore";
import LoaderComp from "@/components/LoaderComp";
import '../../../../../../../custom/activeBtn.css'
import { getAuth } from "firebase/auth";
import Loading from "@/app/(dashboard)/loading";
import dynamic from 'next/dynamic'
import OneSignal from '../../../../../oneSignal'; // Import the OneSignal initialization file

 

const StudentClassworks = dynamic(() => import("../../_components/StudentClassworks")) 
const StudentAnnouncements = dynamic(() => import("../../_components/StudentAnnouncements")) 

const JoinedClassPage = () => {

useEffect(() => {
  OneSignal.Slidedown.promptPush();
},[])

  const db = getFirestore(app)
  const auth = getAuth(app);
  const user = auth.currentUser;

  const params = useParams() as {  classCode: string | string[] };
  console.log(" Params are " , params)
  const classCode = Array.isArray(params.classCode) ? params.classCode[0] : params.classCode;
  const [activeComponent, setActiveComponent] = useState('announcements');
  const [isValidParams, setIsValidParams] = useState(false);
  const [isLoading, setIsLoading] = useState(true); 

  useEffect(() => {
    // Check if className and classCode exist in Firestore
    const checkParams = async () => {
      try{
      const classesRef = collection(db, 'joinedClasses');
      const q = query(classesRef,where('classCode', '==', classCode));
      const snapshot = await getDocs(q);
      setIsValidParams(!snapshot.empty); 
      }
      catch(error){
        console.log("Error " , error )
      }// Set isValidParams based on query result
      finally {
        setIsLoading(false); // Set loading state to false after fetching data
      }
    };
  
    checkParams();
  }, [ classCode]);

  const buttonRefs = {
    announcement: useRef<HTMLButtonElement | null>(null),
    classwork: useRef<HTMLButtonElement | null>(null),
    
  };
  type ComponentKey = 'announcements' | 'classwork' ; 

  const handleClick = (component: ComponentKey) => {
    setActiveComponent(component);
     // Set focus on the clicked button
  };

  
  const renderComponent = () => {
    switch (activeComponent) {
      case 'announcements':
        return <StudentAnnouncements  classCode={classCode}/>;
      case 'classwork':
        return  <StudentClassworks classCode={classCode}/>;
      default:
        return null;
    }
  };

  if (isLoading) {
    return <div><LoaderComp/></div>; // Render loader while data is being fetched
  }

  if (!isValidParams) {
    return (
      <div className="max-w-screen h-svh flex items-center justify-center">
        <span className="text-5xl text-center">Class you are looking for
          <br/> doesnot exist</span>
      </div>
    )
  }

  return (
    <>

    <div  className="h-screen bg-white max-w-screen p-6 mx-auto px-[1.5rem]" >
      {" "}
      <div className="flex items-center w-full justify-center gap-5">
        <Button  ref={buttonRefs.announcement}
        onClick={() => handleClick("announcements")} className={`tracking-normal w-22 relative h-8 bg-white  outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white  ${activeComponent === 'announcements' ? 'active-btn' : ''}`}>
        Teacher Announcements

        </Button>
        <Button  ref={buttonRefs.classwork}
        onClick={() => handleClick("classwork")} className={`tracking-normal w-22 relative h-8 bg-white  outline outline-1 outline-zinc-300 text-zinc-400 hover:text-white  ${activeComponent === 'classwork' ? 'active-btn' : ''}`}>
          Assigned Classwork
        </Button>
       
      
        
      </div>
      <div className="mt-10 pb-10">
        
        {renderComponent()}
       
      </div>
    </div>
    </>
  );
};

export default JoinedClassPage;
