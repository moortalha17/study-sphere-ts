
import type { Metadata, Viewport } from "next";
import NextTopLoader from 'nextjs-toploader';
import "./globals.css";
import { cn } from "../lib/utils";
import localFont from 'next/font/local'
import { Toaster } from "@/components/ui/toaster"


const myFont = localFont({ src: '../assets/fonts/CircularStd-Book.otf' })

export const metadata: Metadata = {
  title: "StudySphere",
  description: "Your own Virtual Classroom",
  
};
 

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  
 
  return (
  
    <html lang="en" >
      
      <meta name="theme-color" content="white"/>

      <body
        className={cn(
          "min-h-screen bg-background antialiased ",
          `${myFont.className}`
        )}
      >
       
        <NextTopLoader
        color="#37986A"
     
  initialPosition={0.08}
  crawlSpeed={200}
  height={4}
  crawl={true}

  easing="ease"
  speed={100}
  shadow="0 0 10px #2299DD,0 0 5px #2299DD"
  zIndex={1600}
  showAtBottom={false}
        />
       
        {children}
       
        <Toaster />
       
      </body>
   
    </html>
  
  );
}
