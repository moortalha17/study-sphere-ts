// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage, ref } from "firebase/storage";
import { getMessaging, getToken } from "firebase/messaging";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAIuQIuzvVfjcWi_2ormEe-nP3g8tl7mkI",
  authDomain: "studyspheredb.firebaseapp.com",
  projectId: "studyspheredb",
  storageBucket: "studyspheredb.appspot.com",
  messagingSenderId: "961382501327",
  appId: "1:961382501327:web:4d7e3772b1be6315c4e0ba",
  measurementId: "G-LM7WS9FG3Q"
};


// Request permission to receive notifications
// Initialize Firebase
export const app = initializeApp(firebaseConfig);





const db = getFirestore(app);

export const storage = getStorage(app)
export const storageRef = ref(storage)



export default db