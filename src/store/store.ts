import { configureStore } from "@reduxjs/toolkit";
import mobileMenuSlice from "@/features/mobilemenu/mobileMenuSlice";
import userStateSlice from "@/features/userstate/userStateSlice";
import formReducer from "@/features/formData/formSlice";
export const store = configureStore({
    reducer:{
        mobileMenu : mobileMenuSlice,
        userState : userStateSlice,
        formData: formReducer,
    },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch


