// formSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface FormDataState {
  classCode: string;
  className: string;
  subject: string;
  section: string;
}

const initialState: FormDataState = {
  classCode: '',
  className: '',
  subject: '',
  section: '',
};

const formSlice = createSlice({
  name: 'formData',
  initialState,
  reducers: {
    setFormData: (state, action: PayloadAction<FormDataState>) => {
      return { ...state, ...action.payload };
    },
    clearFormData: () => initialState,
  },
});

export const { setFormData, clearFormData } = formSlice.actions;
export default formSlice.reducer;
